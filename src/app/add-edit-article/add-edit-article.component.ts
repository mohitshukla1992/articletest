import { Component, OnInit,ElementRef,Input } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgbModal,NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthenticationService } from '../services/authentication-service.service';
import { FormBuilder,FormsModule , FormArray, Validators,ReactiveFormsModule } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
@Component({
  selector: 'app-add-edit-article',
  templateUrl: './add-edit-article.component.html',
  styleUrls: ['./add-edit-article.component.css']
})
export class AddEditArticleComponent implements OnInit {
  articleError: string;
  @Input() formtype:any;
  saveError:any;
  private loggedIn: boolean ;
  constructor( public fb: FormBuilder,
    private allService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private titleService: Title,
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private el: ElementRef
  ) { }
  closeResult: string;
  
  //public uploader:FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});
  ngOnInit() {
    this.loggedIn = false;
  //  //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
  //  this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
  //  //overide the onCompleteItem property of the uploader so we are 
  //  //able to deal with the server response.
  //  this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
  //       console.log("ImageUpload:uploaded:", item, status, response);
  //   };

  }
  
  
  articleForm = this.fb.group({
    title: ['', Validators.required],
    description: ['', Validators.required],
    image:[''],
  });

  get title() { return this.articleForm.get('title'); };
  get description() { return this.articleForm.get('description'); }
  get image() { return this.articleForm.get('image'); }
  
  closeModal(){
    this.activeModal.close(null);
  }
  setPageTitle(title: string) {
    this.titleService.setTitle(title);
  }
       
  onSubmit () {
    
        //locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#image');
        //get the total amount of files attached to the file input.
         let fileCount: number = inputEl.files.length;
         //create a new fromdata instance
         let formData = new FormData();
          //check if the filecount is greater than zero, to be sure a file was selected.
         if (fileCount > 0) { // a file was selected
             //append the key name 'photo' with the first file in the element
                 formData.append('image', inputEl.files.item(0));
                 formData.append('title', this.articleForm.get('title').value);
                 formData.append('description', this.articleForm.get('description').value);
                 if(this.formtype =='add'){
                  this.allService.addArticle(formData).subscribe(
                    res => {
                      
                        if(res.status ==1){
                          this.activeModal.close(null);
                         
                        
                        }else{
                          this.saveError = res.error;
                        }
                        
                      
                    },
                    error => this.saveError = error
                  );
                 }else{
                  this.allService.updateArticle(formData).subscribe(
                    res => {
                      
                        if(res.status ==1){
                          this.activeModal.close(null);
                         
                        
                        }else{
                          this.saveError = res.error;
                        }
                        
                      
                    },
                    error => this.saveError = error
                  );
                 }
                
                
              
          
        }
   
    
  }
}
