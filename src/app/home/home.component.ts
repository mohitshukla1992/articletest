import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddEditArticleComponent } from '../add-edit-article/add-edit-article.component';
import { AuthenticationService } from '../services/authentication-service.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(  private allService: AuthenticationService,private modalService: NgbModal,private titleService: Title) { }
  articles:any;
  error: {};
  ngOnInit() {
  }

  openAddArticleModal() {
    const modalRef = this.modalService.open(AddEditArticleComponent, { centered: true, keyboard: false, size: 'xl' });

   
  }
  openEditArticleModal(){
    const modalRef = this.modalService.open(AddEditArticleComponent, {backdrop: 'static',size: 'lg', keyboard: false, centered: true});
  }
  getArticle(){
    this.allService.articleList().subscribe(
      res => {
        
          if(res.status ==1){
            this.articles = res.data;
          }else{
            console.log('no data found')
          }
          
        
      },
      error => console.log('no data found')
    );
    
  }
  
}
