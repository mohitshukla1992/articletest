import { AddEditArticleComponent } from './add-edit-article/add-edit-article.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component'
import { NgbModule, NgbActiveModal, NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LazyLoadScriptService } from './lazy-load-script.service';
import { AuthenticationService } from './services/authentication-service.service';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { FileSelectDirective } from 'ng2-file-upload';
export function ModalConfig(): NgbModalConfig {
  return Object.assign(new NgbModalConfig(), {
    backdrop: 'static', keyboard: false
  });
}
@NgModule({
  
  declarations: [
    AppComponent,
    HomeComponent,
    AddEditArticleComponent,
    FileSelectDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    LazyLoadScriptService,
    NgbActiveModal,
    AuthenticationService,
    { provide: NgbModalConfig, useFactory: ModalConfig },
  ],
  bootstrap: [AppComponent],
  entryComponents: [AddEditArticleComponent],
})
export class AppModule { }
